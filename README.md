To use this service:
1. get fmi apikey from here: https://ilmatieteenlaitos.fi/rekisteroityminen-avoimen-datan-kayttajaksi
2. go to file customAttributes.js
3. replace text replace-with-your-fmi-apikey with your fmi-apikey
4. save changes

Web pages worth of checking:
http://en.ilmatieteenlaitos.fi/open-data-manual