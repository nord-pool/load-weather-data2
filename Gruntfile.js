module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
      pkg : grunt.file.readJSON('package.json'),
      clean : {
          files : ['lib/**/*','!lib/.gitignore']
      },
      gitclone: {
        clone: {
            options: {
                repository: 'https://gitlab.com/tjnikkila/MetolibNode.git',
                branch: 'master',
                directory: 'lib/metolibnode'
            }
        }
      },
      subgrunt: {
        target0: {
          projects: {
            './lib/metolibnode': 'default'
          }
        }
      }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-git');
    grunt.loadNpmTasks('grunt-subgrunt');
    grunt.registerTask('default', ['clean:files', 'gitclone', 'subgrunt:target0']);
}
