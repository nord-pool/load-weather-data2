var customAttributes = require("./personalConfigurations");

module.exports = {
  serverUrl: getServerUrl,
  storedQueryForecast: "fmi::forecast::hirlam::surface::point::multipointcoverage",
  storedQueryObservation: "fmi::observations::weather::multipointcoverage",
  wfsRequestParserModule: "../lib/metolibnode/src/wfsRequestParserNode",
  hourInMillisec: 1000 * 60 * 60, //todo: relocate all that are not really configurations
  dayInMillisec: 1000 * 60 * 60 * 23, // todo: refactor so that 23 can be 24 or change name
  getData: "getData"
}

function getServerUrl(){
    return "http://data.fmi.fi/fmi-apikey/" + customAttributes.fmiApikey + "/wfs";
}
