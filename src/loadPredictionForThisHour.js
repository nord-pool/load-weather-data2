// https://github.com/fmidev/metolib/blob/master/doc/wfsrequestparser.md
// node loadPrediction.js Helsinki 2016-12-16T03 data preTemp

// muuta niin, että lataa viimeisimmän tiedoston ja vertaa viimeistä kyselyä siihen.
  // jos uusi niin talleta kuten nyt
  // jos 24h +/-2h kohdalta löytyy vastaavat edellisestä
    // tallenna vanhan tiedostoon loppuun aikaleima
var moment = require('moment');
var weatherFunctions = require('./weatherFunctions');
var configurations = require("./configurations");
var wfsRequestParserNode = require(configurations.wfsRequestParserModule);

var city = process.argv[2];
var predictionTime = (new Date()).getTime();
var folderPath = process.argv[3];
var preFileName = process.argv[4];
var fullPath = folderPath + "/" + preFileName;

if (folderPath === undefined || city === undefined){
  console.log("Some info is missing");
  console.log(folderPath, city);
  return false;
}



// new Date("2013-05-10T08:00:00Z")).getTime()
var forecastForComingNHours = 50 *  configurations.hourInMillisec;
var predictUntil = predictionTime + forecastForComingNHours;

var parameters = {
    url : configurations.serverUrl(),
    storedQueryId : configurations.storedQueryForecast,
    requestParameter : "temperature",
    begin : predictionTime,
    end : predictUntil,
    timestep :  configurations.hourInMillisec,
    sites : [city],
    callback : function(data, errors) {
      handleCallback(data, errors, folderPath, preFileName, predictionTime);
    }
};

wfsRequestParserNode(configurations.getData, parameters);

function handleCallback(data, errors, folderPath, preFileName, predictionTime) {
  var fileName = getFileName(data, preFileName, predictionTime);
  var predictions = data.locations[0].data.temperature.timeValuePairs.map(weatherFunctions.timeFromMillisecToUtcStr);

  weatherFunctions.saveToFile(predictions, folderPath, fileName);

  console.error(errors);
}

function getFileName(data, preFileName, predictionTimeMillisec){
  var predDate = new Date(predictionTimeMillisec);
  var dateStr = moment(predDate).utc().format('YYYYMMDDHH');

  return preFileName + data.locations[0].info.country + data.locations[0].info.name +  dateStr + ".csv";
}
