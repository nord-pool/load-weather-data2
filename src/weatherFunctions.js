var fs = require('fs');
var deepcopy = require('deepcopy');

module.exports = {
  timeFromMillisecToUtcStr: timeFromMillisecToUtcStr,
  saveToFile: saveToFile
}

function timeFromMillisecToUtcStr(prediction) {
  var copied = deepcopy(prediction);
  copied["time"] = new Date(prediction.time).toISOString();
  return copied;
}

// todo: remove this. All that calls this should now pring result to strout
function saveToFile(predictions, folderPath, fileName) {
  if (!fs.existsSync(folderPath)){
      fs.mkdirSync(folderPath);
  }
  var stream = fs.createWriteStream(folderPath + "/" + fileName, {
    flags: 'w',
    defaultEncoding: 'utf8'
  });
  stream.once('open', function(fd) {
    predictions.forEach(function(prediciton){
      stream.write(prediciton.time + ";" + prediciton.value + "\n");
    });

    stream.end();
  });
}
