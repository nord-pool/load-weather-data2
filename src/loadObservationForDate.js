// node loadObservationForDate.js Helsinki 2015-01-01 /Users/timonikkila/AtomProjects/WeatherService/src/data hourlyObservation

var fs = require('fs');
var moment = require('moment');
var util = require("util");
var weatherFunctions = require('./weatherFunctions');
var configurations = require("./configurations");
var wfsRequestParserNode = require(configurations.wfsRequestParserModule);

var city = process.argv[2];
// observation date format: 2013-05-10
var observationDateStart = (new Date(process.argv[3] + "T00:00:00Z")).getTime();
var folderPath = process.argv[4];
var preFileName = process.argv[5];
var fullPath = folderPath + "/" + preFileName;

if (folderPath === undefined || city === undefined || isNaN(observationDateStart)){
  throw Error("Some info is missing: folderPaht: {0}, city: {1}, date: {2}".format(folderPath, city, observationDateStart));
}

var parameters = {
  url: configurations.serverUrl(),
  storedQueryId : configurations.storedQueryObservation,
  requestParameter : "td",
  // Integer values are used to init dates for older browsers.
  // (new Date("2013-05-10T08:00:00Z")).getTime()
  // (new Date("2013-05-12T10:00:00Z")).getTime()
  begin : observationDateStart,
  end : observationDateStart + configurations.dayInMillisec,
  timestep :configurations.hourInMillisec,
  sites : city,
  callback : function(data, errors) {
    handleCallback(data, errors, observationDateStart, folderPath, preFileName);
  }
}

wfsRequestParserNode(configurations.getData, parameters);

function handleCallback(data, errors, observationDateStart, folderPath, preFileName) {
  if(errors.length > 0){
    throw Error(errors);
  }

  var observations = data.locations[0].data.td.timeValuePairs.map(weatherFunctions.timeFromMillisecToUtcStr);

  var predDate = new Date(observationDateStart);
  var dateStr = moment(predDate).utc().format('YYYYMMDD');

  var completeResult = {};
  completeResult.timeValuePairs = observations;
  completeResult.orderedDateId = dateStr;
  completeResult.location = {
    name: data.locations[0].info.name,
    city: data.locations[0].info.region,
    coordinates: data.locations[0].info.position,
    id: data.locations[0].info.id,
    geoid: data.locations[0].info.geoid,
    wmo: data.locations[0].info.wmo,
    fmisid: data.locations[0].info.fmisid
  };

  console.log(JSON.stringify(completeResult));
}
